from setuptools import setup, find_packages

setup(
    name='model salesforce',
    version='0.1',
    description='Models for salesforce data',
    packages=find_packages(),
    package_data={'models': ['*.m5o']},
    install_requires=[],
)
